# <roxxmedia recruitment task>

![Semantic description of image](roxx.png "Roxx media")

## Description

👋 Hello! Project made for a recruitment assignment for Roxx Media.

## Task guidelines

- [x] RWD
- [x] Slider
- [x] View menu to mobile and desktop
- [x] Icons in SVG format
- [x] Animation
- [x] Google Font
- [x] Retina friendly

## Installation

```shell
npm install
```

For dev server

```shell
npm run dev
```

For production build

```shell
npm run build
```

## Used libraries

1. swiperjs.com
2. jQuery
3. Gsap
