import "/scss/base.scss";
import { Swiper, Parallax, Navigation, Pagination } from "swiper";
import $ from "jquery";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
Swiper.use([Parallax, Navigation, Pagination]);

$(function () {
  const swiper = new Swiper(".swiper", {
    effect: "creative",
    autoplay: {
      delay: 5000,
      pauseOnMouseEnter: true,
      stopOnLastSlide: true,
    },
    observer: true,
    observeParents: true,
    creativeEffect: {
      prev: {
        shadow: true,
        translate: ["-40%", 0, -1],
        opacity: 0,
      },
      next: {
        opacity: 1,

        translate: ["100%", 0, 0],
      },
    },
    pagination: {
      el: ".header__pagination",
      clickable: true,
      bulletClass: "header__pagination__item",
      bulletActiveClass: "active",
    },
  });

  $(".form__control > input").on("change", function () {
    if ($(this).val()) {
      $(this).addClass("active");
    } else {
      $(this).removeClass("active");
    }
  });
  $(".form__control > input").each(function () {
    if ($(this).val()) {
      $(this).addClass("active");
    } else {
      $(this).removeClass("active");
    }
  });

  $(".navbar__hamburger").on("click", function () {
    $(this).next("ul").addClass("active");
    $(".navbar__close").addClass("active");
    $(this).parent(".navbar__holder").addClass("active");
    $("html").addClass("block-scroll");
    $("body").addClass("block-scroll");
  });
  $(".navbar__close").on("click", function () {
    $(this).parent(".navbar__holder").children("ul").removeClass("active");
    $(this).parent(".navbar__holder").removeClass("active");
    $("html").removeClass("block-scroll");
    $("body").removeClass("block-scroll");
    $(".navbar__close").removeClass("active");
  });

  $("#modalRun").on("click", function () {
    const target = $(this).attr("data-target");
    $(`#${target}`).addClass("active");
  });

  $("#modalClose").on("click", function () {
    $(this).closest(".modal").removeClass("active");
  });

  gsap.registerPlugin(ScrollTrigger);

  const rows = document.querySelectorAll(".animation-row");

  rows.forEach((row) => {
    gsap.fromTo(
      row.children,
      { y: "+=100", opacity: 0 },
      {
        y: 0,
        opacity: 1,
        stagger: 0.8,
        duration: 1,
        ease: "easeInOut",
        scrollTrigger: {
          trigger: row,
          start: "-80% top",
        },
      }
    );
  });

  const gallery = document.querySelectorAll(".gallery__wrapper");

  gallery.forEach((image) => {
    gsap.fromTo(
      image.children,
      { y: "+=100", opacity: 0 },
      {
        y: 0,
        opacity: 1,
        stagger: 0.2,
        duration: 1,
        ease: "easeInOut",
        scrollTrigger: {
          trigger: image,
          start: "-80% top",
        },
      }
    );
  });

  const car = document.getElementById("leftCar");

  gsap.fromTo(
    car,
    { x: "+=100", opacity: 0 },
    {
      x: 0,
      opacity: 1,
      stagger: 0.2,
      duration: 1,
      ease: "easeInOut",
      scrollTrigger: {
        trigger: ".contact__box",
        start: "bottom bottom",
      },
    }
  );
});
